# Implementing and improving the performance of AODV by receive reply method and securing it from Black hole attack

Kelompok : <br>
Irham Muhammad Fadhil - 05111640000085 <br>
Elvega Dewangga - 05111640000149<br>
Mahathir Muhammad Iqbal - 05111640000162<br>
Ilham Perdana Jalasena - 05111640000182 <br>
<br> 

## 1. Konsep <br>
### 1.1 Deskripsi Paper <br>
Implementasi MAODV didasarkan pada paper berikut :

* Judul : **Implementing and improving the performance of AODV by receive reply method and securing it from Black hole attack**
* Penulis : Debarati Roy Choudhurya, Dr.LeenaRaghab, Prof. Nilesh Marathe
* Sumber : https://www.sciencedirect.com/science/article/pii/S1877050915003452


  

### 1.2 Latar Belakang <br>
Routing di jaringan ad hoc menghadapi sejumlah masalah seperti sistem desentralisasi, node yang bergerak, infrastruktur rendah, lebih banyak daya yang dibutuhkan, kapasitas saluran menengah dan terbatas, node jahat, menyebabkan penurunan kinerja routing yang signifikan. Berdasarkan informasi yang di-<i>broadcast</i> oleh tabel routing, node lainnya diperbarui. AODV adalahrotokol routing <i>on-demand</i> dan diinisiasi oleh sumber. Dalam AODV data routing dikumpulkan berdasarkan permintaan, dan rute diputuskan tergantung pada query dalam jaringan.


### 1.3 Dasar Teori <br>
#### 1.3.1 Ad Hoc On-demand Distance Vector Routing Protocol (AODV Routing Protocol) <br>

AODV adalah routing protocol yang termasuk kategori reactive routing protocol. Seperti reactive routing protocol lainnya, AODV hanya menyimpan informasi routing seputar path dan host yang aktif. Didalam AODV, informasi routing disimpan di semua node. Setiap node menyimpan tabel routing next-hop, dimana menyimpan informasi tujuan ke hop berikutnya dimana node tersebut memiliki route tertentu. Didalam AODV, ketika node asal ingin mengirim packet ke tujuan namun tidak ada route yang tersedia, node tersebut akan memulai proses route discovery. Dalam route discovery ada dua tahapan yang dilakukan, yakni:
<ul>
<li><b>Route Request</b> (RREQ), yakni ketika ada node sumber yang ingin mengirimkan paket ke node destination, dan node sumber tersebut masih belum memiliki rute untuk mengirim paket tersebut</li>
![image1](img/rreq.jpg)
<br> 
<br> 
<li><b>Route Reply</b>, (RREP), yakni dibuat oleh node tujuan atau node yang mempunyai rute menuju tujuan</li>	
![image1](img/rrep.jpg)
<br> 
<br> 
<li><b>Route Error (RERR) </b></li>	
![image1](img/rerr.jpg)
<br> 
<br> 
</ul>

### 1.4 Routing Protocol yang Diusulkan<br>
#### Ad-hoc routing
Routing pada jaringan ad-hoc menghadapi berbagai macam kekurangan :
 - sistem yang tidak terpusat 
 - bnode yang bergerak 
 - infrastruktur yang rendah
 - Menggunakan daya yang banyak
 - kapasitas channel yang terbatas 
 - node yang tidak aman, <br>hal tersebut menyebabkan menurunnya performance routing secara signifikan. <br>

Walaupun memiliki beberapa kekurangan tetapi dengan adanya beberapa modifikasi pada Ad-hoc routing protocol kami dapat menghindari kekurangan-kekurangan yang dimilikinya
    
### 1.5 Cara Kerja <br>

![image1](img/img1.jpg)

 Kami Melakukan Modifikasi cara kerja node source dengan fungsi tambahan pre_Request Receive Reply (Packet P). Selain itu, menambahkan tabel baru _RREP_Tab, timer M_WAIT_TIME yang menunjukkan jumlah waktu RREP atau setengahnya dan variabel Mali_node. Pada paper ini RREP disimpan di tabel baru yang bibuat bernama RREP_Tab sampai waktu M_WAIT_TIME. 
 
## 2. Implementasi
### 2.1 Deskripsi Sistem
Implementasi routing protocol AODV dilakukan pada sistem dengan :
*  Sistem Operasi : Linux Ubuntu 16.04
*  Aplikasi yang digunakan :
   * NS-2.35 ([Instalasi](https://docs.google.com/document/d/1cNnfd57Okh_KwuJOJv95aE1d1jWxz2UgYWMCLUgepvU/edit))
* Bahasa C++ dengan memodifikasi algoritma AODV yang sudah ada
* Simulation Environment :
    * TCL script   

## 3. Testing
> Gambar A <br>
![image1](img/img2.jpg) <br>
> Gambar B <br>
![image1](img/img3.jpg)<br>

- Gambar A<br>
PDR AODV turun 60.867% di hadapan serangan black hole attack. Jika memakai solusi dari metode ini meningkat menjadi 60.877% <br>
- Gambar B <br>
Di gambar b ini menunjukan hasil mengalami kenaikan 13,28% . menunjukan bahwa hasil sebelum di serang. Maka akan meningkatkan hasil efek serangan


## 4. Referensi
*  [A Tutorial on the Implementation of Ad-hoc On Demand Distance Vector (AODV) Protocol in Network Simulator (NS-2)](https://drive.google.com/file/d/0B6aQ8IUEyp5NekhLZWVwV0lRNU0/edit)
*  [Ad hoc On-Demand Distance Vector (AODV) Routing](https://www.ietf.org/rfc/rfc3561.txt)


    



